# mlflow_server

Проект для развертывания mlflow tracking server на удаленной или локальной машине.

## Использование

```bash
docker-compose up -d --build
```
